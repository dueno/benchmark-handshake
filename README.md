# benchmark-handshake

A benchmark program performing TLS handshake in memory, without I/O.

## Building

```console
$ make
```

## Running

```console
$ benchmark-handshake -c 10000 -p $(nproc) --priority NORMAL
```

## License

LGPL-2.1-or-later
