/* SPDX-License-Identifier: LGPL-2.1-or-later */

#include <assert.h>
#include <errno.h>
#include <error.h>
#include <getopt.h>
#include <gnutls/gnutls.h>
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Certificates taken from tests/cert-common.h from GnuTLS.  */
static char server_localhost_ca3_cert_pem[] =
	"-----BEGIN CERTIFICATE-----\n"
	"MIIEKDCCApCgAwIBAgIMV6MdMjbIDKHKsL32MA0GCSqGSIb3DQEBCwUAMBIxEDAO\n"
	"BgNVBAMTB3N1YkNBLTMwIBcNMTYwNTEwMDg1MTE4WhgPOTk5OTEyMzEyMzU5NTla\n"
	"MAAwggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDZPXiZqiz3wLuz+B4Z\n"
	"nJuCphLEX7k15NcpamL3+9ea4gXyfeFSHbSaihPauBUcDMVbL/wfkhxYiJRCX7wq\n"
	"HIkJK4En5aEzSDDa6pI/CI5lSbiXdNDGbFLh5b8Guvhfzyy8lDjFNNy3abkfU270\n"
	"tnzFY5mkYwYgjuN/RgPqh0b8McT+xUeN9x4PuSXXmMC1r3v7y4JuMxE8ZzGDhW2a\n"
	"QK5Is6QYv0WELS5hVvB8GdP5XQwTJw4HH5i/YES7TENV2RByzRY8hFQ9SbK5YHHG\n"
	"oszVJIlIuxm5v8N2Ig1cW6t7t3HnuZbDYRDCERMiEigBz8vEZZyFsMLg5Z7JiNKS\n"
	"G/f+ER9CzDJXHgxBctV9EEc2KmRT1P9JeI/xZUOl9lKljc+t8m0Um3Asx5duWm4t\n"
	"cZm7FecnaJiTXD/tEG64qTKWtDuoI7+X9MjHe5lvf2gIJT3CoKW24Rn6O1fc9oCC\n"
	"nVAi0V6FLM4XaG50X9NC666RVEFkXih8THA1gC9m9NJMrD0CAwEAAaOBjTCBijAM\n"
	"BgNVHRMBAf8EAjAAMBQGA1UdEQQNMAuCCWxvY2FsaG9zdDATBgNVHSUEDDAKBggr\n"
	"BgEFBQcDATAPBgNVHQ8BAf8EBQMDB6AAMB0GA1UdDgQWBBQzneEn04vV/OsF/LXH\n"
	"gWlPXjvZ1jAfBgNVHSMEGDAWgBQtMwQbJ3+UBHzH4zVP6SWklOG3oTANBgkqhkiG\n"
	"9w0BAQsFAAOCAYEASbEdRkK44GUb0Y+80JdYGFV1YuHUAq4QYSwCdrT0hwJrFYI2\n"
	"s8+9/ncyzeyY00ryg6tPlKyE5B7ss29l8zcj0WJYsUk5kjV6uCWuo9/rqqPHK6Lc\n"
	"Qx1cONR4Vt+gD5TX0nRNuKaHVbBJARZ3YOl2F3nApcR/8boq+WNKGhGkzFMaKV+i\n"
	"IDpB0ziBUcb+q257lQGKrBuXl5nCd+PZswB//pZCsIkTF5jFdjeXvOvGDjYAr8rG\n"
	"KpoMTskNcBqgi59sJc8djWMbNt+15qH4mSvTUW1caukeJAr4mwHfrSK5k9ezSSp1\n"
	"EpbQ2Rp3xpbCgklhtsKHSJZ43sghZvCOxk8G3bRZ1/lW6sXvIPmLkvoeetTLvqYq\n"
	"t/+gfv4NJuyZhzuJHbxrxBJ3C9QjqTbpiUumeRQHXLa+vZJUKX7ak1KVubKiOC+x\n"
	"wyfgmq6quk5jPgOgMJWLwpA2Rm30wqX4OehXov3stSXFb+qASNOHlEtQdgKzIEX/\n"
	"6TXY44pCGHMFO6Kr\n"
	"-----END CERTIFICATE-----\n";

const gnutls_datum_t server_ca3_localhost_cert = {
	(unsigned char *)server_localhost_ca3_cert_pem,
	sizeof(server_localhost_ca3_cert_pem) - 1
};

static char server_ca3_key_pem[] =
	"-----BEGIN RSA PRIVATE KEY-----\n"
	"MIIG5AIBAAKCAYEA2T14maos98C7s/geGZybgqYSxF+5NeTXKWpi9/vXmuIF8n3h\n"
	"Uh20mooT2rgVHAzFWy/8H5IcWIiUQl+8KhyJCSuBJ+WhM0gw2uqSPwiOZUm4l3TQ\n"
	"xmxS4eW/Brr4X88svJQ4xTTct2m5H1Nu9LZ8xWOZpGMGII7jf0YD6odG/DHE/sVH\n"
	"jfceD7kl15jAta97+8uCbjMRPGcxg4VtmkCuSLOkGL9FhC0uYVbwfBnT+V0MEycO\n"
	"Bx+Yv2BEu0xDVdkQcs0WPIRUPUmyuWBxxqLM1SSJSLsZub/DdiINXFure7dx57mW\n"
	"w2EQwhETIhIoAc/LxGWchbDC4OWeyYjSkhv3/hEfQswyVx4MQXLVfRBHNipkU9T/\n"
	"SXiP8WVDpfZSpY3PrfJtFJtwLMeXblpuLXGZuxXnJ2iYk1w/7RBuuKkylrQ7qCO/\n"
	"l/TIx3uZb39oCCU9wqCltuEZ+jtX3PaAgp1QItFehSzOF2hudF/TQuuukVRBZF4o\n"
	"fExwNYAvZvTSTKw9AgMBAAECggGAel8TMVRYMlOCKJWqtvit7QGJ7s6RMRewUCca\n"
	"iuB1ikyp1vgr1argEnGXT4yEb6GOBpjYKByRFRoSkfUFtJ8QXncAMS48CPwwcRDT\n"
	"wugZ9lp5ve9Sr4NTiOZ3Hd5yjN3SMIQ6GnR1pGfMnSXNidHGJRa+9IfHas2yvv38\n"
	"tL7xMJ0EgBM3BHRgnbDI7VKhs3afm63+0f64RdNHY/PkUpD+2/s9g6czDIq65qAn\n"
	"pXCTJJPSenN0hnS5AYzECtGh2JkFjXpF5B7/2pvZjqsy8eyjZURoQFLA5wWhLVr5\n"
	"AQDJzeK//D6OMAd6kuLKezQxVIN0F0eC6XKEhEvq96xegQk3aMXk2jCHz6IYV6pm\n"
	"zdnfIvP5fIP1HsL8JPiCQqBp7/MoSKlz/DCHH/6iQgQkIhxw/nYJd1+kjhHpm969\n"
	"fw6WzzCA7om0CbKhuHjRnnwk1OylqKhTrgfO1mcaEoH90NIszE3j5pwqiPMdv+J0\n"
	"k25pjaMDgeOd3bO8SW/oWQEH5LbBAoHBAP7QAaYg4Fsm0zr1Jvup6MsJdsI+2aTh\n"
	"4E+hrx/MKsd78mQpRNXvEVIeopp214rzqW/dv/4vMBoV9tRCuw5cJCZCHaeGZ4JF\n"
	"pU/+nBliukanL3XMN5Fp74vVthuQp69u3fa6YHHvL2L6EahSrHrbSE4+C5VYOV+Z\n"
	"nfKDHD9Vo1zH8Fjxl7JJWI/LgSXCChm6Y9Vq7LviL7hZc4BdCbGJfAfv56oGHavE\n"
	"zxU639fBbdhavNl6b9i7AeTD4Ad1KbsFrQKBwQDaQKP0eegbnHfHOdE+mb2aMtVN\n"
	"f3BI25VsBoNWD2A0VEFMQClUPMH17OyS2YidYeVbcneef3VlgrIJZvlRsr76LHxP\n"
	"vVtEug6ZgX5WS/DiJiZWESVJrGZ+gaeUIONGFObGO+Evvoe5bqSwm2Bu05HONb56\n"
	"Q5qx7gfo+kfxHm2vjOOKpc/ceEz2QeJ3rOGoetocmaObHcgFOFO0UC2oyAJ3MAtY\n"
	"8SkyiUJ/jDdCZbkVegT9kGe9OLKMpenG058uctECgcEAozqgM8mPrxR576SnakN3\n"
	"isjvOJOGXGcNiDVst5PUO6Gcrqj5HYpdsBtL0mMaxDo+ahjFKpET4UH8shBlP1er\n"
	"GI717CDfIcZ3lXzmhiSGa0gh0PYXCqGwAAXQ+Gt735fHvIu7yICN/Htw4EDFmJXs\n"
	"BaMdTHgNmL4RPg7bA39afM7fmjp5EI6HmuWkP4nDaqPJ3Cb4q4rDQvaaVLpEwWPu\n"
	"/i6iWno8e5JBjbn/NnkEYroNi8sw5sc0+VS4qE5XgySpAoHBAMB9bF0tu4nGqVl7\n"
	"49FrdO7v0HLGZ/jKOfIJmIIpk3bzrJecqxbRc1v79vbZhwUPl2LdBSU0Uw0RhQaH\n"
	"3HKyzH8HByio4DswQbofnJZt6ej7LqqP+qwMsmT24x7hFrHzs0m4/DXIvBnOvM/K\n"
	"afW1AY62leVthJ1TS4SuYQ8HAERpZTIeZcKUE4TJvPxB7NBUcdPxqXsgfA4mjKSm\n"
	"Zm7K4GnQZOGv6N7aclzeBMq5vtBzSr18RBJ+U/N6TUH/2Q/1UQKBwEPgS+LJCJAs\n"
	"qaeBPTgiuzv2a6umQpezxjCispnU5e0sOFHV/f5NVuEZDrdH7WDHAX8nAU8TdDZM\n"
	"/fqM4oOZJOY9yVsyXK9dN7YcG6lxlNbC8S4FatDorDr3DxmbeYqEMUfOR+H4VvgR\n"
	"OHw+G5gmNHBAh30wDR+bxepSNBAexjo18zbMgNJsdyjU8s562Q7/ejcTgqZYt4nZ\n"
	"r6wql68K+fJ1W38b+ENQ46bZZMvAh8z4MZyzBvS8M/grD0WBBwrWLA==\n"
	"-----END RSA PRIVATE KEY-----\n";

const gnutls_datum_t server_ca3_key = { (unsigned char *)server_ca3_key_pem,
					sizeof(server_ca3_key_pem) - 1 };

static char ca3_cert_pem[] =
	"-----BEGIN CERTIFICATE-----\n"
	"MIID+jCCAmKgAwIBAgIIVzGgXgSsTYwwDQYJKoZIhvcNAQELBQAwDzENMAsGA1UE\n"
	"AxMEQ0EtMzAgFw0xNjA1MTAwODQ4MzBaGA85OTk5MTIzMTIzNTk1OVowDzENMAsG\n"
	"A1UEAxMEQ0EtMzCCAaIwDQYJKoZIhvcNAQEBBQADggGPADCCAYoCggGBALbdxniG\n"
	"+2wP/ONeZfvR7AJakVo5deFKIHVTiiBWwhg+HSjd4nfDa+vyTt/wIdldP1PriD1R\n"
	"igc8z68+RxPpGfAc197pKlKpO08I0L1RDKnjBWr4fGdCzE6uZ/ZsKVifoIZpdC8M\n"
	"2IYpAIMajEtnH53XZ1hTEviXTsneuiCTtap73OeSkL71SrIMkgBmAX17gfX3SxFj\n"
	"QUzOs6QMMOa3+8GW7RI+E/SyS1QkOO860dj9XYgOnTL20ibGcWF2XmTiQASI+KmH\n"
	"vYJCNJF/8pvmyJRyBHGZO830aBY0+DcS2bLKcyMiWfOJw7WnpaO7zSEC5WFgo4jd\n"
	"qroUBQdjQNCSSdrt1yYrAl1Sj2PMxYFX4H545Pr2sMpwC9AnPk9+uucT1Inj9615\n"
	"qbuXgFwhkgpK5pnPjzKaHp7ESlJj4/dIPTmhlt5BV+CLh7tSLzVLrddGU+os8Jin\n"
	"T42radJ5V51Hn0C1CHIaFAuBCd5XRHXtrKb7WcnwCOxlcvux9h5/847F4wIDAQAB\n"
	"o1gwVjAPBgNVHRMBAf8EBTADAQH/MBMGA1UdJQQMMAoGCCsGAQUFBwMJMA8GA1Ud\n"
	"DwEB/wQFAwMHBgAwHQYDVR0OBBYEFPmohhljtqQUE2B2DwGaNTbv8bSvMA0GCSqG\n"
	"SIb3DQEBCwUAA4IBgQBhBi8dXQMtXH2oqcuHuEj9JkxraAsaJvc1WAoxbiqVcJKc\n"
	"VSC0gvoCY3q+NQvuePzw5dzd5JBfkoIsP5U6ATWAUPPqCP+/jRnFqDQlH626mhDG\n"
	"VS8W7Ee8z1KWqnKWGv5nkrZ6r3y9bVaNUmY7rytzuct1bI9YkX1kM66vgnU2xeMI\n"
	"jDe36/wTtBRVFPSPpE3KL9hxCg3KgPSeSmmIhmQxJ1M6xe00314/GX3lTDt55UdM\n"
	"gmldl2LHV+0i1NPCgnuOEFVOiz2nHAnw2LNmvHEDDpPauz2Meeh9aaDeefIh2u/w\n"
	"g39WRPhU1mYvmxvYZqA/jwSctiEhuKEBBZSOHxeTjplH1THlIziVnYyVW4sPMiGU\n"
	"ajXhTi47H219hx87+bldruOtirbDIslL9RGWqWAkMeGP+hUl1R2zvDukaqIKqIN8\n"
	"1/A/EeMoI6/IHb1BpgY2rGs/I/QTb3VTKqQUYv09Hi+itPCdKqamSm8dZMKKaPA0\n"
	"fD9yskUMFPBhfj8BvXg=\n"
	"-----END CERTIFICATE-----\n";

const gnutls_datum_t ca3_cert = { (unsigned char *)ca3_cert_pem,
				  sizeof(ca3_cert_pem) - 1 };

#define MIN(x,y) ((x) < (y) ? (x) : (y))

struct buffer {
	char data[64 * 1024];
	size_t len;
};

struct loopback {
	gnutls_session_t server;
	struct buffer server_buffer;

	gnutls_session_t client;
	struct buffer client_buffer;
};

static ssize_t server_pull(gnutls_transport_ptr_t tr, void *data, size_t len)
{
	struct loopback *loopback = tr;

	if (loopback->server_buffer.len == 0) {
		gnutls_transport_set_errno((gnutls_session_t)loopback->server, EAGAIN);
		return -1;
	}

	len = MIN(len, loopback->server_buffer.len);
	memcpy(data, loopback->server_buffer.data, len);

	memmove(loopback->server_buffer.data, loopback->server_buffer.data + len,
		loopback->server_buffer.len - len);
	loopback->server_buffer.len -= len;

	return len;
}

static ssize_t server_push(gnutls_transport_ptr_t tr, const void *data,
			   size_t len)
{
	struct loopback *loopback = tr;
	size_t newlen;

	len = MIN(len, sizeof(loopback->client_buffer.data) - loopback->client_buffer.len);

	newlen = loopback->client_buffer.len + len;
	memcpy(loopback->client_buffer.data + loopback->client_buffer.len, data, len);
	loopback->client_buffer.len = newlen;

	return len;
}

static ssize_t client_pull(gnutls_transport_ptr_t tr, void *data, size_t len)
{
	struct loopback *loopback = tr;

	if (loopback->client_buffer.len == 0) {
		gnutls_transport_set_errno((gnutls_session_t)loopback->client, EAGAIN);
		return -1;
	}

	len = MIN(len, loopback->client_buffer.len);

	memcpy(data, loopback->client_buffer.data, len);

	memmove(loopback->client_buffer.data, loopback->client_buffer.data + len, loopback->client_buffer.len - len);
	loopback->client_buffer.len -= len;
	return len;
}

static ssize_t client_push(gnutls_transport_ptr_t tr, const void *data,
			   size_t len)
{
	struct loopback *loopback = tr;
	size_t newlen;

	len = MIN(len, sizeof(loopback->server_buffer.data) - loopback->server_buffer.len);

	newlen = loopback->server_buffer.len + len;
	memcpy(loopback->server_buffer.data + loopback->server_buffer.len, data, len);
	loopback->server_buffer.len = newlen;
	return len;
}

static void run(const char *sprio, const char *cprio)
{
	int sret, cret;
	gnutls_certificate_credentials_t scred, ccred;
	gnutls_session_t server, client;
	struct loopback loopback;

	memset(&loopback, 0, sizeof(loopback));

	assert(gnutls_certificate_allocate_credentials(&scred) >= 0);

	assert(gnutls_certificate_set_x509_key_mem(
		       scred, &server_ca3_localhost_cert, &server_ca3_key,
		       GNUTLS_X509_FMT_PEM) >= 0);

	assert(gnutls_certificate_allocate_credentials(&ccred) >= 0);

	assert(gnutls_certificate_set_x509_trust_mem(ccred, &ca3_cert,
						     GNUTLS_X509_FMT_PEM) >= 0);

	assert(gnutls_init(&server, GNUTLS_SERVER) >= 0);
	assert(gnutls_init(&client, GNUTLS_CLIENT) >= 0);

	gnutls_credentials_set(server, GNUTLS_CRD_CERTIFICATE, scred);
	gnutls_credentials_set(client, GNUTLS_CRD_CERTIFICATE, ccred);

	gnutls_transport_set_push_function(server, server_push);
	gnutls_transport_set_pull_function(server, server_pull);
	loopback.server = server;
	gnutls_transport_set_ptr(server, &loopback);
	assert(gnutls_priority_set_direct(server, sprio, 0) >= 0);

	gnutls_transport_set_push_function(client, client_push);
	gnutls_transport_set_pull_function(client, client_pull);
	loopback.client = client;
	gnutls_transport_set_ptr(client, &loopback);
	assert(gnutls_priority_set_direct(client, cprio, 0) >= 0);

	sret = cret = GNUTLS_E_AGAIN;
	do {
		if (cret == GNUTLS_E_AGAIN) {
			cret = gnutls_handshake(client);
			if (cret == GNUTLS_E_INTERRUPTED)
				cret = GNUTLS_E_AGAIN;
		}
		if (sret == GNUTLS_E_AGAIN) {
			sret = gnutls_handshake(server);
			if (sret == GNUTLS_E_INTERRUPTED)
				sret = GNUTLS_E_AGAIN;
		}
	} while ((cret == GNUTLS_E_AGAIN ||
		  (cret == 0 && sret == GNUTLS_E_AGAIN)) &&
		 (sret == GNUTLS_E_AGAIN ||
		  (sret == 0 && cret == GNUTLS_E_AGAIN)));

	assert(cret == 0 && sret == 0);

	gnutls_bye(client, GNUTLS_SHUT_RDWR);
	gnutls_bye(server, GNUTLS_SHUT_RDWR);

	gnutls_deinit(server);
	gnutls_deinit(client);

	gnutls_certificate_free_credentials(scred);
	gnutls_certificate_free_credentials(ccred);
}

struct thread_arg {
	unsigned long count;
	const char *priority;
};

static void *
thread_func(void *arg) {
	struct thread_arg *ta = arg;
	unsigned int i;

	for (i = 0; i < ta->count; i++) {
		run(ta->priority, ta->priority);
	}

	return NULL;
}

static struct option long_options[] = {
	{ "count", required_argument, 0, 'c' },
	{ "parallel", required_argument, 0, 'p' },
	{ "priority", required_argument, 0, CHAR_MAX + 1 },
	{ "help", no_argument, 0, CHAR_MAX + 2 },
	{ 0, 0, 0, 0 },
};

static void
usage(FILE *out)
{
	fprintf(out, "Usage: benchmark-handshake [OPTIONS]\n"
		"where OPTIONS are:\n"
		"\t-c, --count=COUNT\tNumber of handshakes to be performed\n"
		"\t-p, --parallel=PARALLEL\tNumber of threads to be created\n"
		"\t--priority=PRIORITY\tTLS priority string\n");
}

#define MAX_THREADS 64

int main(int argc, char **argv)
{
	char *endptr;
	unsigned long count = 1000, parallel = 1, i;
	pthread_t threads[MAX_THREADS];
	const char *priority = "NORMAL:-VERS-ALL:+VERS-TLS1.2";

	while (true) {
	        int option_index = 0;
		int c = getopt_long(argc, argv, "c:p:",
				    long_options, &option_index);
		if (c == -1) {
			break;
		}
		switch (c) {
		case 'c':
			count = strtoul(optarg, &endptr, 0);
			if (count == ULONG_MAX && errno == ERANGE) {
				error(1, errno, "unable to read argument for --count");
			}
			break;
		case 'p':
			parallel = strtoul(optarg, &endptr, 0);
			if (parallel == ULONG_MAX && errno == ERANGE) {
				error(1, errno, "unable to read argument for --parallel");
			}
			if (parallel == 0 || parallel > MAX_THREADS) {
				error(1, EINVAL, "invalid argument for --parallel");
			}
			break;
		case CHAR_MAX + 1:
			priority = optarg;
			break;
		case CHAR_MAX + 2:
			usage(stdout);
			return EXIT_SUCCESS;
		default:
			usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (optind < argc) {
		usage(stderr);
		return EXIT_FAILURE;
	}

	unsigned long unit = count / parallel;
	unsigned long remainder = count % parallel;

	for (i = 0; i < parallel - 1; i++) {
		struct thread_arg arg;

		arg.count = unit;
		arg.priority = priority;

		pthread_create(&threads[i], NULL, thread_func, &arg);
	}

	for (i = 0; i < unit + remainder; i++) {
		run(priority, priority);
	}

	for (i = 0; i < parallel - 1; i++) {
		pthread_join(threads[i], NULL);
	}

	return EXIT_SUCCESS;
}
